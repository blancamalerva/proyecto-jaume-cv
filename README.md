<div>
    <h1>Proyecto Jaume CV</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](https://www.andreemalerva.com/), en conjunto a [gitlab](https://gitlab.com/andreemalerva/proyecto-jaume-cv), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 hola@andreemalerva.com
📲 +52 2283530727
```

# Acerca del proyecto

Este es un proyecto realizado con buenos hacks de CSS, todos aprendidos en LeonidasEsteban.com .

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://andreemalerva.gitlab.io/proyecto-jaume-cv)🫶🏻

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2023 LYAM ANDREE CRUZ MALERVA
